﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace end2
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[8, 8];
        int m = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(30, 30);
                    btns[i, j].Location = new Point(i * 30, j * 30);
                    btns[i, j].BackColor = Color.White;
                    btns[i, j].Click += new System.EventHandler(btn_click);
                    this.Controls.Add(btns[i, j]);

                }
            }
        }

        public void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
           m = (m + 1) % 2;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (m == 1)
                    {
                       if (btns[i, j].BackColor == Color.Black)
                          btns[i, j].BackColor = Color.Black;
                       if (btns[i, j].BackColor == Color.White)
                           btns[i, j].BackColor = Color.White;
                    }
                    if (m == 0)
                    {
                        if (btns[i, j].BackColor == Color.Black)
                            btns[i, j].BackColor = Color.White;
                       // if (btns[i, j].BackColor == Color.White)
                           // btns[i, j].BackColor = Color.Black;
                    }
                }
            }
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        if (x == i)
                        {
                            btns[x, j].BackColor = Color.Black;
                            btns[j, y].BackColor = Color.Black;
                        }
                    }
                }
               
            /*
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                        if (btns[i, j].BackColor == Color.White)
                            btns[i, j].BackColor = Color.Black;
                }*/

        }
    }
}
