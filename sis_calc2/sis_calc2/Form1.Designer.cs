﻿namespace sis_calc2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.plus = new System.Windows.Forms.Button();
            this.ravno = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.umn = new System.Windows.Forms.Button();
            this.del = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.MR = new System.Windows.Forms.Button();
            this.MC = new System.Windows.Forms.Button();
            this.Mplus = new System.Windows.Forms.Button();
            this.MS = new System.Windows.Forms.Button();
            this.Mmin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(50, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 20);
            this.textBox1.TabIndex = 1;
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(200, 50);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(50, 50);
            this.plus.TabIndex = 2;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // ravno
            // 
            this.ravno.Location = new System.Drawing.Point(250, 50);
            this.ravno.Name = "ravno";
            this.ravno.Size = new System.Drawing.Size(50, 100);
            this.ravno.TabIndex = 3;
            this.ravno.Text = "=";
            this.ravno.UseVisualStyleBackColor = true;
            this.ravno.Click += new System.EventHandler(this.ravno_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(200, 100);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(50, 50);
            this.minus.TabIndex = 4;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // umn
            // 
            this.umn.Location = new System.Drawing.Point(200, 150);
            this.umn.Name = "umn";
            this.umn.Size = new System.Drawing.Size(50, 50);
            this.umn.TabIndex = 5;
            this.umn.Text = "*";
            this.umn.UseVisualStyleBackColor = true;
            this.umn.Click += new System.EventHandler(this.umn_Click);
            // 
            // del
            // 
            this.del.Location = new System.Drawing.Point(250, 150);
            this.del.Name = "del";
            this.del.Size = new System.Drawing.Size(50, 50);
            this.del.TabIndex = 6;
            this.del.Text = "/";
            this.del.UseVisualStyleBackColor = true;
            this.del.Click += new System.EventHandler(this.del_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(16, 16);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(0, 0);
            this.button3.TabIndex = 9;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // MR
            // 
            this.MR.Location = new System.Drawing.Point(300, 50);
            this.MR.Name = "MR";
            this.MR.Size = new System.Drawing.Size(50, 50);
            this.MR.TabIndex = 10;
            this.MR.Text = "MR";
            this.MR.UseVisualStyleBackColor = true;
            this.MR.Click += new System.EventHandler(this.MR_Click);
            // 
            // MC
            // 
            this.MC.Location = new System.Drawing.Point(300, 100);
            this.MC.Name = "MC";
            this.MC.Size = new System.Drawing.Size(50, 50);
            this.MC.TabIndex = 11;
            this.MC.Text = "MC";
            this.MC.UseVisualStyleBackColor = true;
            this.MC.Click += new System.EventHandler(this.MC_Click);
            // 
            // Mplus
            // 
            this.Mplus.Location = new System.Drawing.Point(300, 150);
            this.Mplus.Name = "Mplus";
            this.Mplus.Size = new System.Drawing.Size(50, 50);
            this.Mplus.TabIndex = 12;
            this.Mplus.Text = "M+";
            this.Mplus.UseVisualStyleBackColor = true;
            this.Mplus.Click += new System.EventHandler(this.Mplus_Click);
            // 
            // MS
            // 
            this.MS.Location = new System.Drawing.Point(350, 50);
            this.MS.Name = "MS";
            this.MS.Size = new System.Drawing.Size(50, 50);
            this.MS.TabIndex = 13;
            this.MS.Text = "MS";
            this.MS.UseVisualStyleBackColor = true;
            this.MS.Click += new System.EventHandler(this.MS_Click);
            // 
            // Mmin
            // 
            this.Mmin.Location = new System.Drawing.Point(350, 100);
            this.Mmin.Name = "Mmin";
            this.Mmin.Size = new System.Drawing.Size(50, 50);
            this.Mmin.TabIndex = 14;
            this.Mmin.Text = "M-";
            this.Mmin.UseVisualStyleBackColor = true;
            this.Mmin.Click += new System.EventHandler(this.Mmin_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 370);
            this.Controls.Add(this.Mmin);
            this.Controls.Add(this.MS);
            this.Controls.Add(this.Mplus);
            this.Controls.Add(this.MC);
            this.Controls.Add(this.MR);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.del);
            this.Controls.Add(this.umn);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.ravno);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button ravno;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button umn;
        private System.Windows.Forms.Button del;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button MR;
        private System.Windows.Forms.Button MC;
        private System.Windows.Forms.Button Mplus;
        private System.Windows.Forms.Button MS;
        private System.Windows.Forms.Button Mmin;
    }
}

