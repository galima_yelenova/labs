﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sis_calc2
{
    public partial class Form1 : Form
    {
        char isPressed;
        int a;
        int ind = -1, result, memres;
        int b;
        public Form1()
        {
            InitializeComponent();
            for (int i = 1; i <= 3; i++)
            {
                Button b = new Button();
                b.Location = new Point(i * 50, 50);
                b.Size = new Size(50, 50);
                b.Text = i.ToString();
                this.Controls.Add(b);
                b.Click += new System.EventHandler(bt_click);
            }
            int k = 1;
            for (int i = 4; i <= 6; i++)
            {
                Button b = new Button();
                b.Location = new Point(k * 50, 2 * 50);
                b.Size = new Size(50, 50);
                b.Text = i.ToString();
                this.Controls.Add(b);
                b.Click += new System.EventHandler(bt_click);
                k++;
            }
            int m = 1;
            for (int i = 7; i <= 9; i++)
            {
                Button b = new Button();
                b.Location = new Point(m * 50, 3 * 50);
                b.Size = new Size(50, 50);
                b.Text = i.ToString();
                this.Controls.Add(b);
                b.Click += new System.EventHandler(bt_click);
                m++;
            }
        }
        public void bt_click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            textBox1.Text += b.Text;
        }

        private void plus_Click(object sender, EventArgs e)
        {
            isPressed = '+';
            a = int.Parse(textBox1.Text);
            textBox1.Text += '+';
        }

        private void ravno_Click(object sender, EventArgs e)
        {
            if (isPressed == '+')
            {
                second();
                result = a + b;
                textBox1.Text += result.ToString();
            }
            if (isPressed == '-')
            {
                second();
                result = a - b;
                textBox1.Text += result.ToString();
            }
            if (isPressed == '*')
            {
                second();
                result = a * b;
                textBox1.Text += result.ToString();
            }
            if (isPressed == '/')
            {
                second();
                result = a / b;
                textBox1.Text += result.ToString();
            }
        }

        public void second()
        {
             string s = textBox1.Text;
             string str = "";
                for (int i = 0; i < s.Length-1; i++)
                {
                    if (s[i] == '+' || s[i] == '-' || s[i] == '*' || s[i] == '/')
                        ind = i;
                }
                for (int j = ind+ 1; j < s.Length; j++)
                {
                    str += s[j];
                    b = int.Parse(str);
                    textBox1.Clear();
                }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            isPressed = '-';
            a = int.Parse(textBox1.Text);
            textBox1.Text += '-';
        }

        private void umn_Click(object sender, EventArgs e)
        {
            isPressed = '*';
            a = int.Parse(textBox1.Text);
            textBox1.Text += '*';
        }

        private void del_Click(object sender, EventArgs e)
        {
            isPressed = '/';
            a = int.Parse(textBox1.Text);
            textBox1.Text += '/';
        }

        private void MR_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Text += memres.ToString();
        }

        private void Mplus_Click(object sender, EventArgs e)
        {
            memres += int.Parse(textBox1.Text);
        }

        private void MS_Click(object sender, EventArgs e)
        {
            memres = int.Parse(textBox1.Text);
        }

        private void Mmin_Click(object sender, EventArgs e)
        {
            memres -= int.Parse(textBox1.Text);
        }

        private void MC_Click(object sender, EventArgs e)
        {
            memres = 0;
        }        

    }
}
