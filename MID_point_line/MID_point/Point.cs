﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MID_point
{
    public class Point
    {
        public int x;
        public int y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public static Point operator +(Point a, Point b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }
    }
}
