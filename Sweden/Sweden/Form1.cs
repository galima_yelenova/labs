﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Sweden
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pen p = new Pen(Color.DodgerBlue, 10);
            Graphics g = this.CreateGraphics();
            g.DrawRectangle(p, 200, 200, 400, 250);
            Brush b = new SolidBrush(Color.DodgerBlue);
            g.FillRectangle(b, 200, 200, 400, 250);
            Brush b1 = new SolidBrush(Color.Gold);
            g.FillRectangle(b1, 360, 195, 40, 260);
            Brush b2 = new SolidBrush(Color.Gold);
            g.FillRectangle(b2, 195, 300, 410, 50);
        }
    }
}
