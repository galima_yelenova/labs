﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task2
{
    public partial class Form1 : Form
    {
        Pen p = new Pen(Color.YellowGreen, 7);
        Graphics g;
        int x1 = 120, y1 = 100, x2 = 220, y2 = 100;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        public void Form1_Click(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            g.DrawLine(p, x1, y1, x2, y2);
        }

        private void RIGHT(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            x1 -= 5;
            x2 -= 5;
            g.Clear(Color.WhiteSmoke);
            g.DrawLine(p, x1, y1, x2, y2);


        }

        private void LEFT_Click(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            x1 += 5;
            x2 += 5;
            g.Clear(Color.WhiteSmoke);
            g.DrawLine(p, x1, y1, x2, y2);
        }
    }
}
