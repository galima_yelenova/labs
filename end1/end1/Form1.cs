﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace end1
{
    public partial class Form1 : Form
    {
        int a, d, n, result;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = calc().ToString();
        }
        public int calc()
        {
            a = int.Parse(textBox1.Text);
            d = int.Parse(textBox2.Text);
            n = int.Parse(textBox3.Text);
            result = a + d * (n - 1);
            return result;
        }
    }
}
