﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MID_point
{
    public class Line
    {
        Point x;
        Point y;
        public Line(Point _x, Point _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return x.ToString() + y.ToString();
        }
        public double getLenght(Point a, Point b)
        {
            double z;
            z = Math.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
            return z;
        }
    }
}
