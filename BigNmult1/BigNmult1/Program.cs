﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNmult1
{
    struct BigNumber
    {
        int[] a;
        int sz;
        public BigNumber(string s)
        {
            a = new int[1000];
            sz = s.Length;
            for (int i = sz - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }
        public static BigNumber operator *(BigNumber x, int y)
        {
            int k = 0;
                for (int i = 0; i < x.sz; i++)
                {
                    x.a[i] = x.a[i] * y + k;
                    k = x.a[i] / 10;
                    x.a[i] %= 10;
                }
                if (k > 0)
                {
                    x.sz++;
                    x.a[x.sz - 1] = k;
                }
            return x;
        }

        public override string ToString()
        {
            string s = "";
            for (int i = sz - 1; i >= 0; i--)
            {
                s += a[i].ToString();
            }
            return s;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            int y = int.Parse(Console.ReadLine());
            BigNumber x = new BigNumber(s1);
            BigNumber z = new BigNumber();
            z = x * y * y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
