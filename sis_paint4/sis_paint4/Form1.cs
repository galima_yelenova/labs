﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sis_paint4
{
    public partial class Form1 : Form
    {
        Pen p = new Pen(Color.Black, 1);
        Point start_p, end_p;
        Graphics g_bitmap;
        Bitmap bitmap;
        bool isPressed;
        int tool; //1 - pen 2 - line 3 - rectangle 4 - circle 5 - eraser
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(this.Width, this.Height);
            g_bitmap = Graphics.FromImage(bitmap);
        }

        private void red_Click(object sender, EventArgs e)
        {
            p.Color = red.BackColor;
            defCol.BackColor = red.BackColor;
        }

        private void black_Click(object sender, EventArgs e)
        {
            p.Color = black.BackColor;
            defCol.BackColor = black.BackColor;
        }

        private void white_Click(object sender, EventArgs e)
        {
            p.Color = white.BackColor;
            defCol.BackColor = white.BackColor;
        }

        private void yellow_Click(object sender, EventArgs e)
        {
            p.Color = yellow.BackColor;
            defCol.BackColor = yellow.BackColor;
        }

        private void green_Click(object sender, EventArgs e)
        {
            p.Color = green.BackColor;
            defCol.BackColor = green.BackColor;
        }

        private void blue_Click(object sender, EventArgs e)
        {
            p.Color = blue.BackColor;
            defCol.BackColor = blue.BackColor;
        }

        private void pen_box_Click(object sender, EventArgs e)
        {
            tool = 1;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            isPressed = true;
            start_p = e.Location;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false; 
            if (tool == 1)
            {
                if (tool == 1)
                {
                    end_p = e.Location;
                    g_bitmap.DrawLine(p, start_p, end_p);
                }
                start_p = end_p;
            }
            if (tool == 2)
            {
                end_p = e.Location;
                g_bitmap.DrawLine(p, start_p, end_p);
            }
            if (tool == 3 || tool == 4)
            {
                int x = Math.Min(start_p.X, end_p.X);
                int y = Math.Min(start_p.Y, end_p.Y);
                int w = Math.Abs(start_p.X - end_p.X);
                int h = Math.Abs(start_p.Y - end_p.Y);
                if (tool == 3)
                {
                    g_bitmap.DrawRectangle(p, x, y, w, h);
                }
                if (tool == 4)
                {
                    g_bitmap.DrawEllipse(p, x, y, w, h);
                }
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                if (tool == 1)
                {
                    if (tool == 1)
                    {
                        end_p = e.Location;
                       Graphics g = this.CreateGraphics();
                        //g_bitmap.Clear(Color.WhiteSmoke);
                        //g.DrawImage(bitmap, 0, 0);
                        g_bitmap.DrawLine(p, start_p, end_p);
                        g.DrawImage(bitmap, 0, 0);
                    }
                    start_p = end_p;
                }
                if (tool == 2)
                {
                    end_p = e.Location;
                    Graphics g = this.CreateGraphics();
                    g.Clear(Color.WhiteSmoke);
                    g.DrawImage(bitmap, 0, 0);
                    g.DrawLine(p, start_p, end_p);
                }
                if (tool ==  3 || tool == 4) 
                {
                    end_p = e.Location;
                    Graphics g = this.CreateGraphics();
                    int x = Math.Min(start_p.X, end_p.X);
                    int y = Math.Min(start_p.Y, end_p.Y);
                    int w = Math.Abs(start_p.X - end_p.X);
                    int h = Math.Abs(start_p.Y - end_p.Y);
                    g.Clear(Color.WhiteSmoke);
                    g.DrawImage(bitmap, 0, 0);
                    if (tool == 3)
                    {
                        g.DrawRectangle(p, x, y, w, h);
                    }
                    if (tool == 4)
                    {
                        g.DrawEllipse(p, x, y, w, h);
                    }
                }
            }
        }

        private void line_box_Click(object sender, EventArgs e)
        {
            tool = 2;
        }

        private void circle_box_Click(object sender, EventArgs e)
        {
            tool = 4;
        }

        private void rec_box_Click(object sender, EventArgs e)
        {
            tool = 3;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            p = new Pen(Color.Black, int.Parse(textBox1.Text));
        }
    }
}
