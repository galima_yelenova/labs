﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sisssssssnake
{
    public partial class Form1 : Form
    {

        List<Point> snake = new List<Point>();
        int way = 0;
        Timer timer = new Timer();
        Point apple;
        Random rand = new Random();
        int S = 10;
        
        public Form1()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(Form1_Paint);
            
             
            timer.Interval = 200;
            timer.Tick += new EventHandler(timer_Tick);
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            timer.Enabled = true;
            snake.Add(new Point(150, 150));
            snake.Add(new Point(150, 149));
            snake.Add(new Point(150, 148));

            

            

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillEllipse(Brushes.Red, new Rectangle(apple.X * S, apple.Y * S, S, S));
            e.Graphics.FillRectangle(Brushes.Blue, new Rectangle(snake[0].X * S, snake[0].Y * S, S, S));
            for (int i = 1; i < snake.Count; i++)
                e.Graphics.FillRectangle(Brushes.Black, new Rectangle(snake[i].X * S, snake[i].Y * S, S, S));

        }

        void timer_Tick(object sender, EventArgs e)
        {
            int x = snake[0].X, y = snake[0].Y;

            switch (way)
            {
                case 0:
                    y--;
                    if (y < 0)
                        timer.Enabled = false;
                    break;
                case 1:
                    x++;
                    if (x >= 300)
                        timer.Enabled = false;
                    break;
                case 2:
                    y++;
                    if (y >= 300)
                        timer.Enabled = false;
                    break;
                case 3:
                    x--;
                    if (x < 0)
                        timer.Enabled = false;
                    break;
            }
            Point c = new Point(x, y);
            snake.Insert(0, c);
            if (snake[0].X == apple.X && snake[0].Y == apple.Y)
            {
                apple = new Point(rand.Next(300), rand.Next(300));
                
               
            }
            else
                snake.RemoveAt(snake.Count - 1);
            Invalidate();


        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Up:
                    if (way != 2)
                        way = 0;
                    break;
                case Keys.Right:
                    if (way != 3)
                        way = 1;
                    break;
                case Keys.Down:
                    if (way != 0)
                        way = 2;
                    break;
                case Keys.Left:
                    if (way != 1)
                        way = 3;
                    break;
            }
        }
    }
}
