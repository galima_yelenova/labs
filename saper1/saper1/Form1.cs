﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace saper1
{
    public partial class Form1 : Form
    {
        saper s = new saper(10, 10, 5);
              public Form1()
            
        {
            //saper s = new saper(10,10,5);
            InitializeComponent();
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Name = i.ToString();
                    b.Size = new Size(30, 30);
                    b.Location = new Point(i * 30, j * 30);
                    b.Click += new System.EventHandler(b_Click);
                    this.Controls.Add(b);
                }
            }
        }

        public void b_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int m = s.move(b.x, b.y);
            if (m == -1)
            {
                b.Text = "*".ToString();
            }
            else
                b.Text = m.ToString();
            
        }
    }
}
