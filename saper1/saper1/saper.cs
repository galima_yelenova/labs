﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saper1
{
    class saper
    {
        public int m, n, k;
        int[,] a;
        

        public saper(int _n, int _m, int _k)
        {
            n = _n;
            m = _m;
            k = _k;
            a = new int[n + 2, m + 2];
            create();
        }

        public void create()
        {
            Random rnd = new Random();
            int x, y;
            for (int i = 1; i < k; i++)
            {
                x = rnd.Next(1, n);
                y = rnd.Next(1, m);
                a[x, y] = 1;
            }
        }
        
        public int move(int x, int y)
        {
            if (a[x, y] == 1)
            {
                return -1;
            }
            
                int count = 0;
                for (int i = x - 1; i <= x + 1; i++)
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (a[i, j] == 1) count++;
                    }
            return count;
           
        }


    }
}

