﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace word_corrector
{
    class Program
    {
        static public int mindist(string dict, string word)
        {
            int[,] arr = new int[dict.Length+1, word.Length+1];
            int n = dict.Length;
            int m = word.Length;
            for (int i = 0; i <= n; i++) arr[i,0] = i;
            for (int j = 0; j <= n; j++) arr[j,0] = j;

            for (int i = 1; i <= n; i++) 
            {
                for (int j = 1; j <= m; j++) 
                {
                    if (i != j) 
                        arr[i-1, j-1] = arr[i-1, j-1] + 2;
                    else 
                        arr[i-1, j-1] = arr[i-1, j-1];
                    arr[i,j] = Math.Min(Math.Min(arr[i-1,j]+1, arr[i,j-1]+1), arr[i-1,j-1]);
                    
                }
            }
            return arr[dict.Length, word.Length];
        }
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"c:/asd/dict.txt");
            //string[] dict = new string[10000];
            int sz = 0;
            string[] dict = new string[10];
            while (sr.Peek() != -1)
            {
                dict[sz] = sr.ReadLine();
                sz++;
            }

                //string s = @"back adandonn aback";
            StreamReader s = new StreamReader(@"c:/asd/text.txt");
            while (s.Peek() != -1)
            {
                string wordline = s.ReadLine();
                string[] word = wordline.Split();

                //string[] word = s.Split();
                for (int i = 0; i < word.Length; i++)
                {
                    Console.WriteLine(word[i]);
                    int min_dist = -1;
                    for (int j = 0; j < sz; j++)
                    {
                        if (mindist(word[i], dict[j]) < min_dist || min_dist == -1)
                        {
                            min_dist = mindist(word[i], dict[j]);
                        }
                    }
                    if (min_dist == 0)
                    {
                        Console.WriteLine("good word");
                    }
                    else
                    {
                        for (int j = 0; j < sz; j++)
                        {
                            if (mindist(word[i], dict[j]) == min_dist)
                            {
                                Console.WriteLine(dict[j]);
                            }
                        }
                        Console.WriteLine();
                    }
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
