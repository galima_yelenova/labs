﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QGame
{
    public partial class Form1 : Form
    {
        Graphics g;
        Brush MyBrush = new SolidBrush(Color.Black);
        int x = 10, y = 10, dx = 5, dy = 5;
        int n = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((x + dx >= n || x + dx <= n + 100) && (y + dy == 340))
            {
                dx = -dx;
            }
            if (x + dx == 350)
            {
                

            }
            if (y + dy == 350)
            {
                MessageBox.Show("You lose");
            }
            if (x + dx >= 800 || x + dx <= 0)
            {
                dx = -dx;
            }
            if (y + dy >= 350 || y + dy <= 0)
            {
                dy = -dy;
            }
            x = x + dx;
            y = y + dy;
            Draw();
        }
        private void Draw()
        {
            g.Clear(Color.White);
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Blue, 5);
            g.DrawEllipse(p, x, y, 10, 10);
            g.FillRectangle(MyBrush, n, 340, 100, 20);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Blue, 5);
            
            g.DrawEllipse(p, x, y, 10, 10);
            g.FillRectangle(MyBrush, n, 340, 100, 20);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Right)
            {
                g.FillRectangle(MyBrush, n += 20, 340, 100, 20);
            }
            else if (e.KeyCode == Keys.Left)
            {
                g.FillRectangle(MyBrush, n -= 20, 340, 100, 20);
            }
        }
    }
}
