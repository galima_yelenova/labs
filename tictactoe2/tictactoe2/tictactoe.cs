﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tictactoe2
{
    public class tictactoe
    {
        int m = 1;
        int[,] a = new int[3, 3];
       
        public int move(int x, int y)
        {
            m = (m + 1) % 2;
            if (m == 0)
            { 
                a[x, y] = 1;
            }

            if (m == 1)
            {
                a[x, y] = 2;
            }
            
            return m;
           
        }

        public int win()
        {
            for (int i = 0; i < 3; i++)
            {
                if (a[i, 0] == a[i, 1] && a[i, 1] == a[i, 2] && a[i, 0] != 0)
                    return a[i, 0];
                if (a[0, i] == a[1, i] && a[1, i] == a[2, i] && a[0, i] != 0)
                    return a[0, i];
            }

            if (a[0, 0] == a[1, 1] && a[1, 1] == a[2, 2] && a[0, 0] != 0)
                return a[0, 0];
            if (a[0, 2] == a[1, 1] && a[1, 1] == a[2, 0] && a[0, 2] != 0)
                return a[0, 2];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (a[i, j] == 0)
                        return 0;
                }
            }
            return 3;
        }

        public void restart()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    a[i, j] = 0;
                }
            }
        }
    }
}
