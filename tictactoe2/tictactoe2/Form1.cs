﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe2
{
    public partial class Form1 : Form
    {
        tictactoe t = new tictactoe();
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    MyButton b = new MyButton(i,j);
                    b.Name = i.ToString();
                    b.Size = new Size(100, 100);
                    b.Location = new Point(i*95,j*95);
                    b.Click += new System.EventHandler(b_Click);
                    this.Controls.Add(b);
                    
                }
            }
        }
        
        
        public void b_Click(object sender, EventArgs e) 
        {

            MyButton b = sender as MyButton;
           
            int m = t.move(b.x, b.y);
            if (m == 0)
                b.Text = "x";
            if (m == 1)
                b.Text = "o";
            int w = t.win();
            if (w == 1)
            {
                MessageBox.Show("Первый игрок выиграл");
                
            }
                
            if (w == 2)
                MessageBox.Show("Второй игрок выиграл");
            if (w == 3)
              MessageBox.Show("Ничья");
            
        }
    }
}
